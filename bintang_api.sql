-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2023 at 06:51 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bintang_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `formulir`
--

CREATE TABLE `formulir` (
  `id` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `due_date` datetime NOT NULL,
  `description` text NOT NULL,
  `cost` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_09_13_011002_create_posts_table', 2),
(6, '2023_09_20_024604_add_role_to_users_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`, `expires_at`) VALUES
(1, 'App\\Models\\User', 3, 'MyAuthApp', '84c866ebaf3e837b0dd0c6a68ed684e059ac27679292e357791e59ab0a6cb0eb', '[\"*\"]', NULL, '2023-09-19 21:30:40', '2023-09-19 21:30:40', '2023-09-20 04:30:40'),
(2, 'App\\Models\\User', 3, 'MyAuthApp', 'e6493ae1c15baa42eacad90f37a189efbc176f204c751c8e1c68d9ab474589c6', '[\"*\"]', NULL, '2023-09-19 21:32:04', '2023-09-19 21:32:04', '2023-09-20 04:32:04'),
(3, 'App\\Models\\User', 4, 'MyAuthApp', 'c433df453b66aba5a5745292e91386d97e3572c15a0f17593ed2c3df94d01c0d', '[\"*\"]', NULL, '2023-09-19 21:52:26', '2023-09-19 21:52:26', '2023-09-20 04:52:26'),
(4, 'App\\Models\\User', 4, 'MyAuthApp', '093411fc86163151b10c5b78b5b24283b7453c191586c71495896dde1e7f3a7f', '[\"*\"]', NULL, '2023-09-19 21:54:59', '2023-09-19 21:54:59', '2023-09-20 04:54:59'),
(5, 'App\\Models\\User', 4, 'MyAuthApp', 'c639bef26943415374f5e341115a3bc4210f1f37ff50dc3519004c0a2d14e793', '[\"*\"]', NULL, '2023-09-19 21:55:10', '2023-09-19 21:55:10', '2023-09-20 04:55:10'),
(6, 'App\\Models\\User', 4, 'MyAuthApp', '0795e0cf2951294ca0b2955ebaa01e0348344653e9e05171a039e97c0b990f5b', '[\"*\"]', NULL, '2023-09-19 21:56:14', '2023-09-19 21:56:14', '2023-09-20 04:56:14'),
(7, 'App\\Models\\User', 5, 'MyAuthApp', 'a6d06d942bc91c5a8bac86ce7fddfae4cefdcfe41fc9d5cf47665806e6e643db', '[\"*\"]', NULL, '2023-09-19 22:10:32', '2023-09-19 22:10:32', '2023-09-20 05:10:32'),
(8, 'App\\Models\\User', 6, 'MyAuthApp', 'd0d74924ef13cbd15ced03438d95c2b46befe90c889a04d41e925e7bad1f416e', '[\"*\"]', NULL, '2023-09-19 22:12:58', '2023-09-19 22:12:58', '2023-09-20 05:12:58'),
(9, 'App\\Models\\User', 7, 'MyAuthApp', '77bf91bad0b783eab1b450d46f86ddca15ffd14778378648c66a6705e58116c7', '[\"*\"]', NULL, '2023-09-19 22:16:32', '2023-09-19 22:16:32', '2023-09-20 05:16:32'),
(10, 'App\\Models\\User', 7, 'MyAuthApp', '6dedb679d7c088b1af314c176aa0f1b578c229d527a9d3c30e8dba84adff5784', '[\"*\"]', '2023-09-19 22:16:50', '2023-09-19 22:16:44', '2023-09-19 22:16:50', '2023-09-20 05:16:44'),
(11, 'App\\Models\\User', 6, 'MyAuthApp', '5c1cdd395af4bee4df5429be8ed0da626607fc4951d95cc9b46fc12f012df50e', '[\"*\"]', '2023-09-19 22:17:44', '2023-09-19 22:17:39', '2023-09-19 22:17:44', '2023-09-20 05:17:39'),
(12, 'App\\Models\\User', 6, 'MyAuthApp', '4d2b0f14219e28b5eb76e2e1a646c32f8e3e348f9a36aeffe800e7c38917033e', '[\"*\"]', '2023-09-19 22:18:43', '2023-09-19 22:18:26', '2023-09-19 22:18:43', '2023-09-20 05:18:26'),
(13, 'App\\Models\\User', 8, 'MyAuthApp', '9a28aa0c85407f6f1212e04ab9b38650aec99a639f178a08744324ac214573d1', '[\"*\"]', NULL, '2023-09-19 22:24:08', '2023-09-19 22:24:08', '2023-09-20 05:24:08'),
(14, 'App\\Models\\User', 9, 'MyAuthApp', '07be80fbbd56d60de7cd9d9817464dc8a21f758ff6932f1974752b2825981b40', '[\"*\"]', NULL, '2023-09-19 22:24:30', '2023-09-19 22:24:30', '2023-09-20 05:24:30'),
(15, 'App\\Models\\User', 9, 'MyAuthApp', 'ee756ce0621311e10eddcd7984ec9cda12e7cc00a0ac7e6ce38f0bd6d46de2cc', '[\"*\"]', '2023-09-19 22:25:13', '2023-09-19 22:24:46', '2023-09-19 22:25:13', '2023-09-20 05:24:46'),
(16, 'App\\Models\\User', 7, 'MyAuthApp', '9961a89963e7b780eea4d95410719ffc0c1e07e5cdca58489ecff21b30387252', '[\"*\"]', '2023-09-19 22:25:51', '2023-09-19 22:25:41', '2023-09-19 22:25:51', '2023-09-20 05:25:41'),
(17, 'App\\Models\\User', 7, 'MyAuthApp', '85bb1fa743bc95b7d78214e53e84f36c85fc72573f84246f43c517c97ec1ea96', '[\"*\"]', NULL, '2023-09-19 22:26:18', '2023-09-19 22:26:18', '2023-09-20 05:26:18'),
(18, 'App\\Models\\User', 9, 'MyAuthApp', '14f4ae33b5fc049ee9b93c57f12dbf220ba15d7b6afe18643581c425e5db1e04', '[\"*\"]', '2023-09-19 22:27:11', '2023-09-19 22:26:51', '2023-09-19 22:27:11', '2023-09-20 05:26:51'),
(19, 'App\\Models\\User', 7, 'MyAuthApp', 'd767e4db7e890df1208da221cb302fa8f6276e8d271236d681e20066690cfe51', '[\"*\"]', NULL, '2023-09-19 22:27:31', '2023-09-19 22:27:31', '2023-09-20 05:27:31'),
(20, 'App\\Models\\User', 7, 'MyAuthApp', '4f1ec6e2e32c4ad826eee2d33f6829382311793d9503f14e67048671735b97bb', '[\"*\"]', '2023-09-19 22:28:37', '2023-09-19 22:28:18', '2023-09-19 22:28:37', '2023-09-20 05:28:18'),
(21, 'App\\Models\\User', 7, 'MyAuthApp', 'af3234b19e3f86ccc97746d180976bfcf1ea995408f5767e34a6423055a08919', '[\"*\"]', '2023-09-19 22:32:48', '2023-09-19 22:32:28', '2023-09-19 22:32:48', '2023-09-20 05:32:28'),
(22, 'App\\Models\\User', 9, 'MyAuthApp', '8d0775ed957c015b2c270f18729f604ff4f6fd79782519bec359ce3db992a4ac', '[\"*\"]', '2023-09-19 22:33:18', '2023-09-19 22:33:05', '2023-09-19 22:33:18', '2023-09-20 05:33:05'),
(23, 'App\\Models\\User', 7, 'MyAuthApp', '937cb36543ba3aecacef1400716cf7387f52ae5b2cedf948645f7375bf0e557f', '[\"*\"]', '2023-09-19 22:35:10', '2023-09-19 22:34:45', '2023-09-19 22:35:10', '2023-09-20 05:34:45'),
(24, 'App\\Models\\User', 9, 'MyAuthApp', 'a461cda60506a5920d22f427cfeb9f8e61c7f5299f199dcc8c2888b58e1b54b6', '[\"*\"]', '2023-09-19 22:35:38', '2023-09-19 22:35:21', '2023-09-19 22:35:38', '2023-09-20 05:35:21'),
(25, 'App\\Models\\User', 10, 'MyAuthApp', '16ec1de6be70fea029a1a70874e514cfe1a9e46cb3a7bc8611c6fae1d7287457', '[\"*\"]', NULL, '2023-09-19 22:36:59', '2023-09-19 22:36:59', '2023-09-20 05:36:59'),
(26, 'App\\Models\\User', 10, 'MyAuthApp', '9b0c8b2be60d79ff9cd834cc8ef48c1dd266d9495e0b8219064bddcc6ebc01d8', '[\"*\"]', '2023-09-19 22:37:39', '2023-09-19 22:37:27', '2023-09-19 22:37:39', '2023-09-20 05:37:27'),
(27, 'App\\Models\\User', 7, 'MyAuthApp', 'f019dc648dbeda55414f84e800161887dafb2188ed74ca135e26cec5132b7b1a', '[\"*\"]', '2023-09-19 22:38:26', '2023-09-19 22:38:14', '2023-09-19 22:38:26', '2023-09-20 05:38:14'),
(28, 'App\\Models\\User', 9, 'MyAuthApp', '59bbe784b303ca0c143e20b1c93a638d2ee7e542f27460b0680b3787b501b544', '[\"*\"]', NULL, '2023-09-19 22:55:15', '2023-09-19 22:55:15', '2023-09-20 05:55:15'),
(29, 'App\\Models\\User', 9, 'MyAuthApp', 'f20e6499af8e277f731688845660210ea795ce76cd22ccad22758cc47b6ceb92', '[\"*\"]', NULL, '2023-09-19 22:55:51', '2023-09-19 22:55:51', '2023-09-20 05:55:51'),
(30, 'App\\Models\\User', 9, 'MyAuthApp', '763d75b7843798be9dac5999b2026d16344f689d1dc7f26536e2880effc57019', '[\"*\"]', NULL, '2023-09-19 22:56:06', '2023-09-19 22:56:06', '2023-09-20 05:56:06'),
(31, 'App\\Models\\User', 9, 'MyAuthApp', '2e7e7d9bf819aad82c96ea4062e7c76b9797c5eb8f945a1bd201af4a66d3d542', '[\"*\"]', '2023-09-19 22:57:08', '2023-09-19 22:56:55', '2023-09-19 22:57:08', '2023-09-20 05:56:55'),
(32, 'App\\Models\\User', 7, 'MyAuthApp', '901bc129062beb9cc05837129fe7c1ee2c3ee8983d6dce127137962cf57101b9', '[\"*\"]', '2023-09-19 22:57:32', '2023-09-19 22:57:22', '2023-09-19 22:57:32', '2023-09-20 05:57:22'),
(33, 'App\\Models\\User', 11, 'MyAuthApp', 'bce9470c9c5e972c35dd9766d5ab8b58408601c2a86f9ee435290f8a9ae1e016', '[\"*\"]', NULL, '2023-09-19 22:58:25', '2023-09-19 22:58:25', '2023-09-20 05:58:25'),
(34, 'App\\Models\\User', 11, 'MyAuthApp', '337748c66053a1ad88828367812371adbfa4b808a3990dc7add658886c3c945c', '[\"*\"]', '2023-09-19 23:01:46', '2023-09-19 22:59:59', '2023-09-19 23:01:46', '2023-09-20 05:59:59'),
(35, 'App\\Models\\User', 12, 'MyAuthApp', '70a5e41116b3e5c4841f2ad40c6242fe2e41e9972219401f26e7805c897b27bd', '[\"*\"]', NULL, '2023-09-19 23:03:08', '2023-09-19 23:03:08', '2023-09-20 06:03:08'),
(36, 'App\\Models\\User', 12, 'MyAuthApp', 'e824ebeffe599cf9554fd69019b933c95fe7124ac49918e611e43d3173d22058', '[\"*\"]', '2023-09-19 23:04:25', '2023-09-19 23:03:43', '2023-09-19 23:04:25', '2023-09-20 06:03:43');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(11, 'bintang', 'binn2@gmail.com', NULL, '$2y$10$LbZ1uCtK86o5J7dAXZskbO3RJfduPC/ku5F4W0qrVuN9PNkiZmTUm', 'user', NULL, '2023-09-19 22:58:25', '2023-09-19 22:58:25'),
(12, 'bintangg', 'binn1@gmail.com', NULL, '$2y$10$doQV9bELtdJSkelGTA.Js.kISnxFRUUIMGSW3uNN7dTHHhbIGFJYS', 'admin', NULL, '2023-09-19 23:03:08', '2023-09-19 23:03:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `formulir`
--
ALTER TABLE `formulir`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
