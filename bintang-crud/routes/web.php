<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layouts.home');
// });
Route::get('/', function () {
    return view('welcome');
});

// Route::get('/layout', function () {
//     return view('Layouts.main');
// });

Route::get('/formulir/create', function () {
    return view('formulir.create');
});

Route::resource('/formulir', \App\Http\Controllers\FormulirController::class);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');