<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FormulirController extends Controller
{
    public function index()
    {
        $data = DB::table('formulir')->get();
        return view('formulir.index', compact('data'));
    }

    public function create()
    {
        return view('formulir.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'due_date' => 'required',
            'cost' => 'required',
            'description' => 'required'
        ]);

        $image = $request->file('photo');
        $image->storeAs('public/formulir', $image->hashName());

        DB::table('formulir')->insert([
            'id' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
            'photo' => $image->hashName(),
            'due_date' => $request->due_date,
            'description' => $request->description,
            'cost' => $request->cost
        ]);

        return redirect()->route('formulir.index')->with(['success' => 'Data Berhasil Disimpan']);
    }

    public function edit($id)
    {
        $data = DB::table('formulir')->where('id', $id)->first();
        return view('formulir.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'due_date' => 'required',
            'cost' => 'required',
            'description' => 'required'
        ]);

        $image = $request->file('photo');
        $image->storeAs('public/formulir', $image->hashName());

        DB::table('formulir')
            ->where('id', $id)
            ->update([
                'photo' => $image->hashName(),
                'due_date' => $request->due_date,
                'description' => $request->description,
                'cost' => $request->cost
            ]);

        return redirect()->route('formulir.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    public function destroy($id)
    {
        DB::table('formulir')->where('id', $id)->delete();
        return redirect()->route('formulir.index')->with(['success' => 'Data Berhasil Dihapus!']);
    }
}
